# Civo Cluster Management

This project help you to 
- Create and manage a Civo Kubernetes cluster from **[Gitpod](https://www.gitpod.io/)**. It uses the **Civo CLI** to create the cluster on **[Civo](https://www.civo.com)**

## Prerequisites

### Accounts

- Create an account on **[Civo](https://www.civo.com)**
- Create an account on **[Gitpod](https://www.gitpod.io)**

### On Gitpod

- Set the `CIVO_API_KEY` environment variable in the GitPod's settings of your GitPod account

### Open this project with Gitpod

- Update the `.env` file of this project with the appropriate values (Civo settings):

  ```bash
  CLUSTER_NAME="panda"
  CLUSTER_SIZE="g3.k3s.large"
  CLUSTER_NODES=1
  CLUSTER_REGION=NYC1
  KUBECONFIG=./config/k3s.yaml
  ```

To create the Kubernetes Civo cluster and get the config file to connect to the cluster with `kubectl`, run the `create-cluster.sh` script. This script will:

- Create the Kubernetes cluster on Civo
- Get the KUBECONFIG file

Once the cluster created, you can check the configuration with these commands:

```bash
kubectl version 
kubectl get nodes
kubectl get pods --all-namespaces
```

or run **K9s** from the Gitpod terminal:

```bash
k9s --all-namespaces
```
