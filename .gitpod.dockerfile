FROM gitpod/workspace-full

RUN sudo apt-get update && \
    sudo apt-get install -y sshpass && \
    brew install derailed/k9s/k9s && \
    brew install helm && \
    brew install kubernetes-cli helm && \
    brew install kompose

USER gitpod

RUN brew install httpie && \
    brew install hey && \
    brew install exa && \
    brew install bat



